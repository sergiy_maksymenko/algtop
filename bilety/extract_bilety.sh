#!/bin/bash

filepdf=bilety_alg_top.pdf
filelatex=bilety_alg_top.tex
biletsdir=bilety

mkdir -p ${biletsdir}
latexmk -pdf ${filelatex}
for i in $(seq -w 10)
do
    pdfjam --landscape --paper a5paper --outfile ${biletsdir}/${i}.pdf -- ${filepdf} $i
done